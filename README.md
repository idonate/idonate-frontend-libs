# What is idonate-frontend-libs?
An early attempt at building a shared, internal library for iDonate frontend projects.

This project is being deprecated in favor of the public-facing `idonate-sdk` library.
