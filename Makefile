# Makefile provided to unify build commands across projects - complex build steps in this project should use node.

help:
	@cat Makefile

release: clean build publish

build: deps
	cd payments && npm run build

clean:
	rm -rf payments/dist/

publish:
	cd payments && npm publish --access public

deps:
	@which npm
	cd payments && npm install
