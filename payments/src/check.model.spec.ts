import { expect } from "chai";
import "mocha";

import { Check } from "./check.model";

describe("Check Credit Card Validity", () => {
  it("Should return initial invalid, but possible, check", () => {
    const testMethod = new Check({});
    expect(testMethod.valid).to.equal(false);
    expect(testMethod.validParts).to.deep.equal({
      account: { valid: false, possible: true },
      routing: { valid: false, possible: true },
    });
  });
  it("Should return invalid, but possible, routing number", () => {
    const testMethod = new Check({});
    testMethod.routing = "02";
    expect(testMethod.valid).to.equal(false);
    expect(testMethod.validParts).to.deep.equal({
      account: { valid: false, possible: true },
      routing: { valid: false, possible: true },
    });
  });
  it("Should return valid routing number", () => {
    const testMethod = new Check({});
    testMethod.routing = "021000021";
    expect(testMethod.valid).to.equal(false);
    expect(testMethod.validParts).to.deep.equal({
      account: { valid: false, possible: true },
      routing: { valid: true, possible: true },
    });
  });
  it("Should return impossible routing number", () => {
    const testMethod = new Check({});
    testMethod.routing = "021000021111sdtgjsrtjsdhsrethjsrtj";
    expect(testMethod.valid).to.equal(false);
    expect(testMethod.validParts).to.deep.equal({
      account: { valid: false, possible: true },
      routing: { valid: false, possible: false },
    });
  });
  it("Should return invalid, but possible, account number", () => {
    const testMethod = new Check({});
    testMethod.account = "98";
    expect(testMethod.valid).to.equal(false);
    expect(testMethod.validParts).to.deep.equal({
      account: { valid: false, possible: true },
      routing: { valid: false, possible: true },
    });
  });
  it("Should return valid account number", () => {
    const testMethod = new Check({});
    testMethod.account = "9876543210";
    expect(testMethod.valid).to.equal(false);
    expect(testMethod.validParts).to.deep.equal({
      account: { valid: true, possible: true },
      routing: { valid: false, possible: true },
    });
  });
  it("Should return impossible account number", () => {
    const testMethod = new Check({});
    testMethod.account = "98765432101113987629696";
    expect(testMethod.valid).to.equal(false);
    expect(testMethod.validParts).to.deep.equal({
      account: { valid: false, possible: false },
      routing: { valid: false, possible: true },
    });
  });
});
