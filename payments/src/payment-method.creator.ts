import { PaymentMethod } from "./payment-method.model";

export abstract class PaymentMethodCreator {
  public abstract createPaymentMethod(newMethod: PaymentMethod): Promise<PaymentMethod>;
}
