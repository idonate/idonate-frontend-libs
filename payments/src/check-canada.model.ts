/// <reference path="local-types/declarations.d.ts"/>
import { IPaymentMethodConstructorParameters, PaymentMethod } from "./payment-method.model";

export interface ICheckCanadaConstructorParameters extends IPaymentMethodConstructorParameters {
  account?: string;
  transit?: string;
  bank?: string;
  backendName?: string;
  iatsProcessId?: string;
  gatewayId?: string;
}

export class CheckCanada extends PaymentMethod {
  private account: string = "";
  private transit: string = "";
  private bank: string = "";
  private backendName: string = "";
  private iatsProcessId: string = "";
  private gatewayId: string = "";

  constructor({ firstName, lastName, id, transit, account, bank, displayName, email, street, street2, city, state, zip, country, backendName, iatsProcessId, gatewayId }: ICheckCanadaConstructorParameters) {
    super({firstName, lastName, id, displayName, email, street, street2, city, state, zip, country});
    this.bank = bank || "";
    this.account = account || "";
    this.transit = transit || "";
    this.backendName = backendName || "";
    this.iatsProcessId = iatsProcessId || "";
    this.gatewayId = gatewayId || "";
  }

  public toString() {
    return `CheckCanada {
  firstName: '${this.firstName}',
  lastName: '${this.lastName}',
  id: '${this.id}',
  valid: ${this._valid},
  bank: '${this.bank}',
  account: '${this.account}'
  transit: '${this.transit}'
}`;
  }
}
