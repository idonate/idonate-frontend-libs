import { PaymentMethod } from "./payment-method.model";

export class Paypal extends PaymentMethod {
  isPaypal: boolean;

  constructor(fields) {
    super(fields);
    this.isPaypal = true;
    this.validate();
  }


  public toString() {
    return `Paypal {
  valid: ${this._valid},
}`;
  }

  private validate() {
    this._valid = true;
    // TODO : i think this should validate contact fields?
  }
}
