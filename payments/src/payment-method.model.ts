export interface IPaymentMethodConstructorParameters {
  firstName?: string;
  lastName?: string;
  id?: string;
  displayName?: string;
  email?: string;
  street?: string;
  street2?: string;
  city?: string;
  state?: string;
  zip?: string;
  country?: string;
}

export abstract class PaymentMethod {
  public firstName: string = "";
  public lastName: string = "";
  public email: string = "";
  public street: string = "";
  public street2: string = "";
  public city: string = "";
  public state: string = "";
  public zip: string = "";
  public country: string = "";
  public readonly displayName: string;
  protected _valid: boolean = false;
  private _id: string = "";

  public get valid() {
    return this._valid;
  }

  public get id() {
    return this._id;
  }

  constructor({ firstName, lastName, id, displayName, email, street, street2, city, state, zip, country }: IPaymentMethodConstructorParameters) {
    this.firstName = firstName || "";
    this.lastName = lastName || "";
    this._id = id || "";
    this.displayName = displayName || "";
    this.email = email || "";
    this.street = street || "";
    this.street2 = street2 || "";
    this.city = city || "";
    this.state = state || "";
    this.zip = zip || "";
    this.country = country || "";
  }
}
