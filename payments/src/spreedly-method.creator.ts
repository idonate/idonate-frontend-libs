import axios from "axios";

import { Check } from "./check.model";
import { CreditCard } from "./credit-card.model";
import { PaymentMethodCreator } from "./payment-method.creator";
import { PaymentMethod } from "./payment-method.model";
import { Paypal } from "./paypal.model";

interface ISpreedlyFormat {
}

interface ISpreedlyCreditCardFormat extends ISpreedlyFormat {
  payment_method: {
    credit_card: {
      first_name: string,
      last_name: string,
      month: string,
      number: string,
      verification_value: string,
      year: string,
      email?: string,
      address1?: string,
      address2?: string,
      city?: string,
      state?: string,
      zip?: string,
      country?: string,
    },
    retained?: boolean,
  };
}

interface ISpreedlyCheckFormat extends ISpreedlyFormat {
  payment_method: {
    bank_account: {
      bank_account_number: string,
      bank_routing_number: string,
      bank_account_holder_type: string,
      bank_account_type: string,
      first_name: string,
      last_name: string,
      email?: string,
      address1?: string,
      address2?: string,
      city?: string,
      state?: string,
      zip?: string,
      country?: string,
    },
    retained?: boolean,
  };
}

interface ISpreedlyPaypalFormat  {
  payment_method_type: 'paypal',
  first_name: string,
  last_name: string,

  email?: string,
  address1?: string,
  address2?: string,
  city?: string,
  state?: string,
  zip?: string,
  country?: string,
}

function delay(time: number) {
  return new Promise(resolve => setTimeout(resolve, time));
}

export class SpreedlyMethodCreator extends PaymentMethodCreator {
  private _environmentKey: string;
  private _baseUrl: string;

  constructor(environmentKey: string, embedApiBaseUrl: string) {
    super();
    this._environmentKey = environmentKey;
    this._baseUrl = embedApiBaseUrl;
  }

  public createPaymentMethod(newMethod: PaymentMethod): Promise<PaymentMethod> {
    if (newMethod.valid) {
      const spreedlyFormattedMethod = this.formatMethod(newMethod);
      return this.sendToSpreedly(spreedlyFormattedMethod);
    }
    return Promise.reject(["invalid"]);
  }

  private formatMethod(method: PaymentMethod): ISpreedlyFormat {
    if (method instanceof CreditCard) {
      return this.formatCreditCard(method as CreditCard);
    } else if (method instanceof Check) {
      return this.formatCheck(method as Check);
    } else if (method instanceof Paypal) {
      return this.formatPaypal(method as Paypal);
    }
    throw new Error("Unsupported payment type");
  }

  private formatCreditCard(method: CreditCard): ISpreedlyCreditCardFormat {
    return {
      payment_method: {
        credit_card: {
          first_name: method.firstName,
          last_name: method.lastName,
          month: `${method.expMonth}`,
          number: method.number,
          verification_value: method.cvv,
          year: `${method.expYear}`,
          email: method.email,
          address1: method.street,
          address2: method.street2,
          city: method.city,
          state: method.state,
          zip: method.zip,
          country: method.country,
        },
      },
    };
  }

  private formatCheck(method: Check): ISpreedlyCheckFormat {
    return {
      payment_method: {
        bank_account: {
          bank_account_number: method.account,
          bank_routing_number: method.routing,
          bank_account_holder_type: 'personal',
          bank_account_type: 'checking',
          first_name: method.firstName,
          last_name: method.lastName,
          email: method.email,
          address1: method.street,
          address2: method.street2,
          city: method.city,
          state: method.state,
          zip: method.zip,
          country: method.country,
        },
      },
    };
  }

  private formatPaypal(method: Paypal): ISpreedlyPaypalFormat {
    return {
      payment_method_type: 'paypal',
      first_name: method.firstName,
      last_name: method.lastName,
      email: method.email,
      address1: method.street,
      address2: method.street2,
      city: method.city,
      state: method.state,
      zip: method.zip,
      country: method.country,
    };
  }

  public pollForResult(donationId: string, opts): Promise<any> {
    // delays by intervalMs and then repeats every intervalMs until response.processed is truthy
    const resolvedOpts = {
      intervalMs: 2000,
      waitForTransaction: false,

      ...(opts || {})
    };

    function isReady(response): boolean {
      // feel free to refactor this if/when more opts are added

      if (
          !response.processed
          || (resolvedOpts.waitForTransaction && !response.transaction)
      ) {
        return false;
      }

      return true;
    }

    return new Promise((resolve, reject) => {
      // wrap body in a timeout - no need to bring in more promises here via delay(...)
      setTimeout(() => {
        let pollHandle;

        pollHandle = setInterval(() => {
          axios.get(`${this._baseUrl}donate/cash-payment/${donationId}/payment_status`)
              // unwrap api response into expected layout
              .then((rawResponse: any) => rawResponse.data.result)
              // process
              .then((response: any) => {
                if (isReady(response)) {
                  // result is ready

                  if (pollHandle) {
                    // cancel interval
                    clearInterval(pollHandle);
                    pollHandle = null;
                  }

                  if (response.error) {
                    // reject errors
                    reject(new Error(response.error));
                  }

                  // resolve successes
                  resolve(response);
                } else {
                  // response is not ready - do nothing, interval will come around again
                }
              })
        }, resolvedOpts.intervalMs);
      }, resolvedOpts.intervalMs);
    });
  }

  private sendToSpreedly(method: ISpreedlyFormat): Promise<PaymentMethod> {
    return axios.post("https://core.spreedly.com/v1/payment_methods.json", {
      ...method,
      environment_key: this._environmentKey,
    }).then((response: any) => {
      if (!response.data.transaction.succeeded) {
        throw new Error(response.data.transaction.message);
      } else {
        const newMethod = response.data.transaction.payment_method;
        if (newMethod.payment_method_type === "credit_card") {
          return new CreditCard({
            cvv: newMethod.verification_value,
            expMonth: newMethod.month,
            expYear: newMethod.year,
            firstName: newMethod.first_name,
            id: newMethod.token,
            lastName: newMethod.last_name,
            number: newMethod.number,
          });
        } else if (newMethod.payment_method_type === "bank_account") {
          return new Check({
            account: newMethod.account_number,
            firstName: newMethod.first_name,
            id: newMethod.token,
            lastName: newMethod.last_name,
            routing: newMethod.routing_number,
          });
        } else if (newMethod.payment_method_type === "paypal") {
          return new Paypal({
            firstName: newMethod.first_name,
            lastName: newMethod.last_name,
            id: newMethod.token,
          })
        } else {
          throw new Error("Unsupported Method Type");
        }
      }
    }).catch((error) => {
      return error.response.data.errors;
    });
  }
}
