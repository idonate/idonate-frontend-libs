import { expect } from "chai";
import "mocha";

import { CreditCard } from "./credit-card.model";

describe("Check Credit Card Validity", () => {
  it("Should return initial invalid, but possible, card", () => {
    const testMethod = new CreditCard({});
    expect(testMethod.valid).to.equal(false);
    expect(testMethod.validParts).to.deep.equal({
      cvv: { valid: false, possible: true },
      expiration: { valid: false, possible: true },
      number: { valid: false, possible: true },
    });
    expect(testMethod.type).to.equal(null);
  });
  it("Should return valid card number", () => {
    const testMethod = new CreditCard({});
    testMethod.number = "4111111111111111";
    expect(testMethod.valid).to.equal(false);
    expect(testMethod.validParts).to.deep.equal({
      cvv: { valid: false, possible: true },
      expiration: { valid: false, possible: true },
      number: { valid: true, possible: true },
    });
  });
  it("Should return invalid card number", () => {
    const testMethod = new CreditCard({});
    testMethod.number = "41111111111111111";
    expect(testMethod.valid).to.equal(false);
    expect(testMethod.validParts).to.deep.equal({
      cvv: { valid: false, possible: true },
      expiration: { valid: false, possible: true },
      number: { valid: false, possible: true },
    });
  });
  it("Should return impossible card number", () => {
    const testMethod = new CreditCard({});
    testMethod.number = "41111111111111111111";
    expect(testMethod.valid).to.equal(false);
    expect(testMethod.validParts).to.deep.equal({
      cvv: { valid: false, possible: true },
      expiration: { valid: false, possible: true },
      number: { valid: false, possible: false },
    });
  });
  it("Should return valid cvv", () => {
    const testMethod = new CreditCard({});
    testMethod.cvv = "111";
    expect(testMethod.valid).to.equal(false);
    expect(testMethod.validParts).to.deep.equal({
      cvv: { valid: true, possible: true },
      expiration: { valid: false, possible: true },
      number: { valid: false, possible: true },
    });
  });
  it("Should return imposssible cvv", () => {
    const testMethod = new CreditCard({});
    testMethod.cvv = "1111";
    expect(testMethod.valid).to.equal(false);
    expect(testMethod.validParts).to.deep.equal({
      cvv: { valid: false, possible: false },
      expiration: { valid: false, possible: true },
      number: { valid: false, possible: true },
    });
  });
  it("Should return invalid cvv", () => {
    const testMethod = new CreditCard({});
    testMethod.cvv = "11";
    expect(testMethod.valid).to.equal(false);
    expect(testMethod.validParts).to.deep.equal({
      cvv: { valid: false, possible: true },
      expiration: { valid: false, possible: true },
      number: { valid: false, possible: true },
    });
  });
  it("Should return valid expiration", () => {
    const testMethod = new CreditCard({});
    testMethod.expMonth = 12;
    testMethod.expYear = 2020;
    expect(testMethod.valid).to.equal(false);
    expect(testMethod.validParts).to.deep.equal({
      cvv: { valid: false, possible: true },
      expiration: { valid: true, possible: true },
      number: { valid: false, possible: true },
    });
  });
  it("Should return impossible expiration", () => {
    const testMethod = new CreditCard({});
    testMethod.expMonth = 12;
    testMethod.expYear = 2017;
    expect(testMethod.valid).to.equal(false);
    expect(testMethod.validParts).to.deep.equal({
      cvv: { valid: false, possible: true },
      expiration: { valid: false, possible: false },
      number: { valid: false, possible: true },
    });
  });
  it("Should return visa card type", () => {
    const testMethod = new CreditCard({});
    testMethod.number = "4111111111111111";
    expect(testMethod.type).to.equal("visa");
  });
});
