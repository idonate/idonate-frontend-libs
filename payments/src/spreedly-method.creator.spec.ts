import axios from "axios";
import mockAdapter from "axios-mock-adapter";
import { expect } from "chai";
import "mocha";

import { Check } from "./check.model";
import { CreditCard } from "./credit-card.model";
import { PaymentMethod } from "./payment-method.model";
import { SpreedlyMethodCreator } from "./spreedly-method.creator";

describe("Check Spreedly method creation", () => {
  it("should return a resolved promise", (done) => {
    const mock = new mockAdapter(axios);
    mock.onPost("https://core.spreedly.com/v1/payment_methods.json").reply(201, {
      transaction: {
        message: "Succeeded!",
        payment_method: {
          card_type: "visa",
          first_name: "Test",
          last_name: "Name",
          month: 12,
          number: "XXXX-XXXX-XXXX-1111",
          payment_method_type: "credit_card",
          token: "SGvOXr61V7AzME654Moe7mpQh7t",
          verification_value: "XXX",
          year: 2020,
          country: 'US',
        },
        succeeded: true,
      },
    });
    const testMethod: PaymentMethod = new CreditCard({
      cvv: "111",
      expMonth: 12,
      expYear: 2020,
      firstName: "Test",
      lastName: "Name",
      number: "4111111111111111",
    });
    const resultPromise = new SpreedlyMethodCreator("FwG83Fl5ikrAMU7H6aaLqTDrdNA", '').createPaymentMethod(testMethod);
    resultPromise.then( (finalResult) => {
      expect(finalResult).to.deep.include({_cvv: "XXX", _number: "XXXX-XXXX-XXXX-1111"});
      expect(finalResult.id).to.not.be.null;
      mock.restore();
      done();
    })
    .catch((error) => {
      mock.restore();
      done(error);
    });
  });
  it("should return a resolved promise", (done) => {
    const mock = new mockAdapter(axios);
    mock.onPost("https://core.spreedly.com/v1/payment_methods.json").reply(201, {
      transaction: {
        message: "Succeeded!",
        payment_method: {
          account_number: "*3210",
          first_name: "Test",
          last_name: "Name",
          payment_method_type: "bank_account",
          routing_number: "021*",
          token: "GCqceAMAjChjE4q96qXuBeT7Mzu",
        },
        succeeded: true,
      },
    });
    const testMethod: PaymentMethod = new Check({
      account: "9876543210",
      firstName: "Test",
      lastName: "Name",
      routing: "021000021",
    });
    const resultPromise = new SpreedlyMethodCreator("FwG83Fl5ikrAMU7H6aaLqTDrdNA", '').createPaymentMethod(testMethod);
    resultPromise.then( (finalResult) => {
      expect(finalResult).to.deep.include({routing: "021*", account: "*3210"});
      expect(finalResult.id).to.not.be.null;
      mock.restore();
      done();
    })
    .catch((error) => {
      mock.restore();
      done(error);
    });
  });
  it("Should return a rejected promise", (done) => {
    const testMethod: PaymentMethod = new CreditCard({});
    const resultPromise = new SpreedlyMethodCreator("FwG83Fl5ikrAMU7H6aaLqTDrdNA", '').createPaymentMethod(testMethod);
    resultPromise
    .then( (result) => done(result))
    .catch((error) => {
      expect(error).to.deep.equal(["invalid"]);
      done();
    });
  });
});
