/// <reference path="local-types/declarations.d.ts"/>
import * as validator from "bank-routing-number-validator";

import { IPaymentMethodConstructorParameters, PaymentMethod } from "./payment-method.model";

export interface ICheckConstructorParameters extends IPaymentMethodConstructorParameters {
  account?: string;
  routing?: string;
}

export class Check extends PaymentMethod {
  private _account: string = "";
  private _routing: string = "";

  private _validParts = {
    routing: { valid: false, possible: true },
  };

  constructor({ firstName, lastName, id, routing, account, displayName, email, street, street2, city, state, zip, country }: ICheckConstructorParameters) {
    super({firstName, lastName, id, displayName, email, street, street2, city, state, zip, country});
    this._routing = routing || "";
    this._account = account || "";
    this.validate();
  }

  get routing(): string { return this._routing; }
  set routing(newRouting: string) {
    this._routing = newRouting;
    this.validate();
  }

  get account(): string { return this._account; }
  set account(newAccount: string) {
    this._account = newAccount;
    this.validate();
  }

  get validParts() { return this._validParts; }

  public toString() {
    return `Check {
  firstName: '${this.firstName}',
  lastName: '${this.lastName}',
  id: '${this.id}',
  valid: ${this._valid},
  routing: '${this._routing}',
  account: '${this._account}'
}`;
  }

  private validate() {
    const validRouting = validator.ABARoutingNumberIsValid(this._routing);
    this._validParts = {
      routing: { possible: validRouting, valid: validRouting },
    };
    if (this._validParts.routing.valid) {
      this._valid = true;
    } else { this._valid = false; }
  }
}
