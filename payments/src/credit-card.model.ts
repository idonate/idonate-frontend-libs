import * as validator from "card-validator";

import { IPaymentMethodConstructorParameters, PaymentMethod } from "./payment-method.model";

export interface ICreditCardConstructorParameters extends IPaymentMethodConstructorParameters {
  number?: string;
  cvv?: string;
  expMonth?: number;
  expYear?: number;
}

export class CreditCard extends PaymentMethod {
  private _number: string = "";
  private _cvv: string = "";
  private _type: string | null = null;
  private _expMonth: number = 0;
  private _expYear: number = 0;

  private _validParts = {
    cvv: { valid: false, possible: false },
    expiration: { valid: false, possible: false },
    number: { valid: false, possible: false },
  };

  constructor({ firstName, lastName, id, number, cvv, expMonth, expYear, displayName, email, street, street2, city, state, zip, country }: ICreditCardConstructorParameters) {
    super({firstName, lastName, id, displayName, email, street, street2, city, state, zip, country});
    this._number = number || "";
    this._cvv = cvv || "";
    this._expMonth = expMonth || 0;
    this._expYear = expYear || 0;
    this.validate();
  }

  get number(): string { return this._number; }
  set number(newNumber: string) {
    this._number = newNumber;
    this.validate();
  }

  get cvv(): string { return this._cvv; }
  set cvv(newCvv: string) {
    this._cvv = newCvv;
    this.validate();
  }

  get type(): string | null { return this._type; }
  set type(newType: string | null) {
    this._type = newType;
    this.validate();
  }

  get expMonth(): number { return this._expMonth; }
  set expMonth(newExpMonth: number) {
    this._expMonth = newExpMonth;
    this.validate();
  }

  get expYear(): number { return this._expYear; }
  set expYear(newExpYear: number) {
    this._expYear = newExpYear;
    this.validate();
  }

  get validParts() { return this._validParts; }

  public toString() {
    return `CreditCard {
  firstName: '${this.firstName}',
  lastName: '${this.lastName}',
  id: '${this.id}',
  valid: ${this._valid},
  number: '${this.number}',
  cvv: '${this.cvv}',
  type: '${this._type}',
  expMonth: ${this._expMonth},
  expYear: ${this._expYear}
}`;
  }

  private validate() {
    const validNumber = validator.number(this.number);
    const validExpiration = validator.expirationDate({ month: `${this._expMonth}`, year: `${this._expYear}` });

    if (validNumber.isValid && validExpiration.isValid && Boolean(this._cvv)) {
      this._valid = true;
      this._validParts = {
        cvv: { valid: true, possible: true },
        expiration: { valid: true, possible: true },
        number: { valid: true, possible: true },
      };
    } else {
      this._valid = false;
      this._validParts = {
        cvv: { possible: Boolean(this._cvv), valid: Boolean(this._cvv) },
        expiration: { possible: validExpiration.isPotentiallyValid, valid: validExpiration.isValid },
        number: { possible: validNumber.isPotentiallyValid, valid: validNumber.isValid },
      };
    }
    if (validNumber.card) {
      this._type = validNumber.card.type;
    }
  }
}
