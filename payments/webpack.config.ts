import * as path from "path";
import * as webpack from "webpack";

const config: webpack.Configuration = {
  devtool: "inline-source-map",
  entry: "./src/index",
  module: {
    rules: [
      {
        exclude: /node_modules/,
        test: /\.tsx?$/,
        use: "ts-loader",
      },
    ],
  },
  output: {
    filename: "idonate-payments.js",
    library: "idonate-payments",
    libraryTarget: "umd",
    path: path.resolve(__dirname, "dist"),
  },
  resolve: {
    extensions: [ ".tsx", ".ts", ".js" ],
  },
};

export default config;
